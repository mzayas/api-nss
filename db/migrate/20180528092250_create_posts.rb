class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.string :title
      t.string :text
      t.timestamps
      t.belongs_to :user, index: true
    end
    
  end
end
