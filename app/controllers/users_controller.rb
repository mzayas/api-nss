class UsersController < ApplicationController
  def index
    @users = User.all
    render json: {data: @users}, status: 201
  end

  def create
    @user = User.new(create_params)

    if @user.save
      render json: {data: @user.as_json(representation: :public)}, status: 201
    else
      render json: {errors: @user.errors}, status: 422
    end

  end

  def create_params
    params.permit(:name, :email)
  end

end
