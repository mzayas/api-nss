class PostsController < ApplicationController
  def index
    @posts = User.find(params[:user_id]).posts
    render json: {data: @posts}, status: 201
  end

  def create
    @post = Post.new(create_params)


    if @post.save
      render json: {data: @post}, status: 201
    else
      render json: {errors: @post.errors}, status: 422
    end

  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    render json: {}, status: 204
  end

  def create_params
    params.permit(:title, :text, :user_id)
  end
end
