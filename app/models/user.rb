class User < ApplicationRecord
  has_many :posts
  
  include AsJsonRepresentations
  
  representation :public do |options| # you can pass options
    {
      name: name,
      email: email
    }
  end

end
